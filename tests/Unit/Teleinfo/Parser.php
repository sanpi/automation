<?php

namespace Test\Unit\Teleinfo;

class Parser extends \atoum
{
    private $parser;

    public function beforeTestMethod($testMethod)
    {
        $this->parser = new \Teleinfo\Parser();
    }

    public function testCreate()
    {
        $this->object($this->parser)
            ->isInstanceOf('\Teleinfo\Parser');
    }

    public function testRead()
    {
        $start = chr(0x2);
        $end = chr(0x3);
        $trame = "ADCO 130622778433 E\rOPTARIF HC.. <\rISOUSC 45 ?\rHCHC 032448321 !\rHCHP 052057919 9\rPTEC HC..\rIINST 001\rIMAX 039 K\rPAPP 00170 )\rHHPHC D /\rMOTDETAT 000000 B";

        $contents = "MOTDETAT 000000 B\r{$end}\r{$start}\r{$trame}\r${end}\r${start}\rADCO 130622778433 E";

        $fd = $this->createFile($contents);

        $this->string($this->parser->read($fd))
            ->isIdenticalTo(<<<EOD
ADCO 130622778433 E
OPTARIF HC.. <
ISOUSC 45 ?
HCHC 032448321 !
HCHP 052057919 9
PTEC HC..
IINST 001
IMAX 039 K
PAPP 00170 )
HHPHC D /
MOTDETAT 000000 B
EOD
            );

    }

    public function testReadInvalidResource()
    {
        $this->exception(function () {
            $this->parser->read('/dev/null');
            })
            ->hasMessage('Invalid resource')
            ->isInstanceOf('\LogicException');
    }

    public function testParse()
    {
        $frame = <<<EOD
ADCO 130622778433 E
OPTARIF HC.. <
ISOUSC 45 ?
HCHC 032448321 !
HCHP 052057919 9
PTEC HC..
IINST 001
IMAX 039 K
PAPP 00170 )
HHPHC D /
MOTDETAT 000000 B
EOD;

        $this->array($this->parser->parse($frame))
            ->isIdenticalTo([
                'adco' => '130622778433',
                'optarif' => 'HC..',
                'isousc' => '45',
                'hchc' => '032448321',
                'hchp' => '052057919',
                'ptec' => 'HC..',
                'iinst' => '001',
                'imax' => '039',
                'papp' => '00170',
                'hhphc' => 'D',
                'motdetat' => '000000',
            ]);
    }

    public function testParseInvalidFrame()
    {
        $frame = <<<EOD
ADCO 130622778433 A
OPTARIF HC.. <
ISOUSC 45 ?
HCHC 032448321 !
HCHP 052057919 9
PTEC HC..
IINST 001
IMAX 039 K
PAPP 00170 )
HHPHC D /
MOTDETAT 000000 B
EOD;

        $this->exception(function () use($frame) {
                $this->parser->parse($frame);
            })
            ->hasMessage("Invalid data: 'ADCO 130622778433 A'")
            ->isInstanceOf('\RuntimeException');
    }

    private function createFile($contents)
    {
        $fd = fopen('php://memory', 'rwb');
        fwrite($fd, $contents);
        rewind($fd);

        return $fd;
    }
}
