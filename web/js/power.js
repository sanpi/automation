"use strict";

var amountFn = function (d) {
    return d.papp;
};

d3.json('/api/teleinfo?start=' + start + '&end=' + end, function (data) {
    var svg = d3.select("svg#power");
    var height = svg.attr("height") - margin.top - margin.bottom;
    var width = svg.attr("width") - margin.left - margin.right;

    var graph = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var [x, xAxis] = createX(width, data, dateFn);
    var [y, yAxis] = createY(height, d3.min(data, amountFn), d3.max(data, amountFn));

    appendX(graph, height, xAxis);
    appendY(graph, width, yAxis, "Consommation (W)");

    var area = d3.svg.area()
        .x(function(d) { return x(dateFn(d)); })
        .y0(height)
        .y1(function(d) { return y(amountFn(d)); });

    graph.append("path")
        .datum(data)
        .attr("class", "area hp")
        .attr("d", area.y1(function (d) {
            return d.ptec === "HP.." ? y(amountFn(d)) : height;
        }));

    graph.append("path")
        .datum(data)
        .attr("class", "area hc")
        .attr("d", area.y1(function (d) {
            return d.ptec === "HC.." ? y(amountFn(d)) : height;
        }));
});
