"use strict";

var color = d3.scale.category10();

var temperatureFn = function (d) {
    return d.temperature_outdoor;
};

var precipitationFn = function (d) {
    return d.rain_1h;
};

d3.json('/api/weather?start=' + start + '&end=' + end, function (data) {
    var svg = d3.select("svg#weather");
    var height = svg.attr("height") - margin.top - margin.bottom;
    var width = svg.attr("width") - margin.left - margin.right;

    var graph = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var [x, xAxis] = createX(width, data, dateFn);

    var min = d3.min([
        d3.min(data, function (d) { return d.temperature_indoor; }),
        d3.min(data, function (d) { return d.temperature_outdoor; })
    ]);
    var max = d3.max([
        d3.max(data, function (d) { return d.temperature_indoor; }),
        d3.max(data, function (d) { return d.temperature_outdoor; })
    ]);
    var [y, yAxis] = createY(height, min, max);

    appendX(graph, height, xAxis);
    appendY(graph, width, yAxis, "Température (°C)");

    var [y2, yAxis2] = createY(height, d3.min(data, precipitationFn), d3.max(data, precipitationFn), "right");
    appendY(graph, width, yAxis2, "Précipitation (mm)", "right");

    var types = ['rain_1h', 'temperature_indoor', 'temperature_outdoor'];
    color.domain(types);
    var temperatures = color.domain().map(function(type) {
        return {
            type: type,
            values: data.map(function(d) {
                return {date: dateFn(d), temperature: d[type]};
            })
        };
    });

    var area = d3.svg.area()
        .x(function(d) { return x(dateFn(d)); })
        .y0(height)
        .y1(function(d) { return y2(precipitationFn(d)); });

    graph.append("path")
        .datum(data)
        .attr("class", "precipitation")
        .attr("d", area.y1(function (d) {
            return y2(precipitationFn(d));
        }));

    var line = d3.svg.line()
        .interpolate("basis")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.temperature); })

    var temperature = graph.selectAll(".temperature")
        .data(temperatures)
        .enter()
            .append("g")
            .attr("class", "temperature");

    temperature.append("path")
        .attr("class", "line")
        .attr("d", function(d) { return line(d.values); })
        .style("stroke", function(d) { return color(d.type); });

    var legend = svg.selectAll(".legend")
        .data(types)
        .enter()
            .append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) {
                return "translate(0," + i * 20 + ")";
            });

    legend.append("rect")
        .attr("x", width)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    legend.append("text")
        .attr("x", width - 6)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) { return d; });
});
