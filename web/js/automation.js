"use strict";

window.location.get = function (name, defaultValue) {
    var value = new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')
        .exec(location.search)

    if (value) {
        return decodeURIComponent(value[1]);
    }
    else {
        return defaultValue;
    }
};

var dateFn = function (d) {
    var date = d.created.date;
    if (date.indexOf('.') > 0) {
        date = date.substring(0, date.indexOf('.'));
    }
    return d3.time.format("%Y-%m-%d %H:%M:%S")
        .parse(date);
};

function createX(width, data, fn)
{
    var x = d3.time.scale()
        .range([0, width])
        .domain(d3.extent(data, fn));

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    return [x, xAxis];
}

function createY(height, min, max, orientation)
{
    if (typeof orientation === "undefined") {
        orientation = "left";
    }

    var y = d3.scale.linear()
        .range([height, 0])
        .domain([min, max]);

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient(orientation);

    return [y, yAxis];
}

function appendX(svg, height, xAxis)
{
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
}

function appendY(svg, width, yAxis, label, orientation)
{
    if (typeof orientation === "undefined") {
        orientation = "left";
    }

    svg.append("g")
        .attr("transform", "translate(" +  (orientation === "left" ? 0 : width) + ",0)")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", orientation === "left" ? ".71em" : "-.91em")
            .style("text-anchor", "end")
            .text(label);

}

var margin = {top: 20, right: 30, bottom: 30, left: 50};
var start = window.location.get('start', '-1day');
var end = window.location.get('end', 'now');
