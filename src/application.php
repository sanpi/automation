<?php

use Symfony\Component\HttpFoundation\Request;

$app = require __DIR__ . '/bootstrap.php';

$app->get('/', function (Request $request) use($app) {
    return $app['twig']->render('index.html.twig');
});

function getModel($app, $table)
{
    $modelName = 'Model\\' . ucfirst($table);
    if (!class_exists($modelName)) {
        $app->abort(404, "Data '$table' doesn't exist");
    }
    return $app['db']->getModel($modelName);
}

$app->get('/api/{table}/{field}', function (Request $request, $table, $field) use($app) {
    $start = $request->query->get('start', '-1 day');
    $end = $request->query->get('end', 'now');

    $rows = getModel($app, $table)->findWhere(
        'created >= $* AND created <= $*',
        [new \DateTime($start), new \DateTime($end)]
    );

    if ($field !== null) {
        if (!isset($rows->get(0)[$field])) {
            $app->abort(404, "Data '$table/$field' doesn't exist");
        }

        $data = [];
        foreach ($rows as $row) {
            $data['labels'][] = $row['created']->format('Y-m-d H:i:s');
            $data['data'][] = $row[$field];
        }
    }
    else {
        $data = $rows;
    }

    return $app->json($data);
})->value('field', null);

$app->get('/api/{table}/{field}/total/{period}', function (Request $request, $table, $field, $period) use($app) {
    $start = $request->query->get('start', '-1 day');
    $end = $request->query->get('end', 'now');

    $rows = getModel($app, $table)
        ->getTotal($field, $period, $start, $end);

    return $app->json($rows);
});

return $app;
