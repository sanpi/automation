CREATE TABLE teleinfo (
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() PRIMARY KEY,
  adco VARCHAR(12) NOT NULL,
  optarif VARCHAR(4) NOT NULL,
  isousc SMALLINT NOT NULL DEFAULT 0,
  hchp BIGINT NOT NULL DEFAULT 0,
  hchc BIGINT NOT NULL DEFAULT 0,
  ptec VARCHAR(4) NOT NULL,
  iinst INT NOT NULL DEFAULT 0,
  imax INT NOT NULL DEFAULT 0,
  papp INTEGER NOT NULL DEFAULT 0,
  hhphc VARCHAR(1) NOT NULL,
  motdetat VARCHAR(6) NOT NULL
);

CREATE TABLE weather (
    created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() PRIMARY KEY,
    temperature_indoor NUMERIC NOT NULL,
    temperature_outdoor NUMERIC NOT NULL,
    dewpoint NUMERIC NOT NULL,
    humidity_indoor INTEGER NOT NULL,
    humidity_outdoor INTEGER NOT NULL,
    wind_all NUMERIC NOT NULL,
    winddir NUMERIC NOT NULL,
    directions VARCHAR(12) NOT NULL,
    windchill NUMERIC NOT NULL,
    rain_1h NUMERIC NOT NULL,
    rain_24h NUMERIC NOT NULL,
    rain_total NUMERIC NOT NULL,
    rel_pressure NUMERIC NOT NULL,
    tendency VARCHAR(12) NOT NULL,
    forecast VARCHAR(12) NOT NULL
);
