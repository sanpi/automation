<?php

namespace Teleinfo;

class Parser
{
    public function read($fd)
    {
        $frame = '';
        $state = 'init';

        if(!is_resource($fd)) {
            throw new \LogicException('Invalid resource');
        }

        while(!feof($fd)) {
            $line = stream_get_line($fd, 256, "\r");

            if(empty($line)) {
                continue;
            }

            switch($state) {
                case 'init':
                    $segment = strstr($line, chr(0x2));
                    if($segment !== false) {
                        $frame .= trim($segment, chr(0x2)) . "\n";
                        $state = 'save';
                    }
                    break;
                case 'save':
                    $segment = strstr($line, chr(0x3), true);
                    if($segment !== false) {
                        $frame .= trim($segment, chr(0x3)) . "\n";
                        $state = 'stop';
                    }
                    else {
                        $frame .= trim($line) . "\n";
                    }
                    break;
                case 'stop':
                    break 2;
                default:
                    throw new \LogicException('Invalid state.');
                    break;
            }
        }

        return trim($frame);
    }

    public function parse($frame)
    {
        $info = [];

        foreach(explode("\n", $frame) as $line) {
            list($name, $value) = explode(' ', $line, 2);
            if(strchr($value, ' ')) {
                list($value, $checksum) = explode(' ', $value);

                $calculedChecksum = $this->getChecksum($name, $value);
                if($calculedChecksum !== $checksum) {
                    throw new \RuntimeException("Invalid data: '$line'");
                }
            }

            $info[strtolower($name)] = $value;
        }
        return $info;
    }

    private function getChecksum($name, $value)
    {
        $checksum = 0;
        $data = "$name $value";

        for($i = 0; $i < strlen($data); $i++) {
            $checksum += ord($data{$i});
        }

        return chr(($checksum & 0x3F) + 0x20);
    }
}
