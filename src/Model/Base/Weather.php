<?php

namespace Model\Base;

use PommProject\ModelManager\Model\FlexibleEntity;

/**
 * Weather
 *
 * Flexible entity for relation
 * public.weather
 *
 * @see FlexibleEntity
 */
class Weather extends FlexibleEntity
{
}
