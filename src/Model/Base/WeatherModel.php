<?php

namespace Model\Base;

use PommProject\ModelManager\Model\Model;
use PommProject\ModelManager\Model\Projection;
use PommProject\ModelManager\Model\ModelTrait\WriteQueries;

use PommProject\Foundation\Where;

use Model\Base\AutoStructure\Weather as WeatherStructure;
use Model\Base\Weather;

/**
 * WeatherModel
 *
 * Model class for table weather.
 *
 * @see Model
 */
class WeatherModel extends Model
{
    use WriteQueries;

    /**
     * __construct()
     *
     * Model constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->structure = new WeatherStructure;
        $this->flexible_entity_class = "\Model\Base\Weather";
    }
}
