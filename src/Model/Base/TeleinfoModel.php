<?php

namespace Model\Base;

use PommProject\ModelManager\Model\Model;
use PommProject\ModelManager\Model\Projection;
use PommProject\ModelManager\Model\ModelTrait\WriteQueries;

use PommProject\Foundation\Where;

use Model\Base\AutoStructure\Teleinfo as TeleinfoStructure;
use Model\Base\Teleinfo;

/**
 * TeleinfoModel
 *
 * Model class for table teleinfo.
 *
 * @see Model
 */
class TeleinfoModel extends Model
{
    use WriteQueries;

    /**
     * __construct()
     *
     * Model constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->structure = new TeleinfoStructure;
        $this->flexible_entity_class = "\Model\Base\Teleinfo";
    }
}
