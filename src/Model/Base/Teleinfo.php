<?php

namespace Model\Base;

use PommProject\ModelManager\Model\FlexibleEntity;

/**
 * Teleinfo
 *
 * Flexible entity for relation
 * public.teleinfo
 *
 * @see FlexibleEntity
 */
class Teleinfo extends FlexibleEntity
{
}
