<?php

namespace Model;

use Model\Base\TeleinfoMap as BaseTeleinfoMap;
use Model\Teleinfo;
use \Pomm\Exception\Exception;
use \Pomm\Query\Where;

class TeleinfoMap extends BaseTeleinfoMap
{
    public function getTotal($field, $period, $start, $end)
    {
        $sql = <<<EOD
SELECT date_trunc(':period', created) AS created, sum(:field) AS :field
    FROM :relation
    WHERE created >= $* AND created <= $*
    GROUP BY date_trunc(':period', created)
EOD;

        $sql = strtr($sql, [
            ':field' => $field,
            ':period' => $period,
            ':relation' => $this->getStructure()->getRelation(),
        ]);

        return $this->query(
            $sql,
            [new \DateTime($start), new \DateTime($end)]
        );
    }
}
