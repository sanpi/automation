<?php

namespace Model;

class Teleinfo extends Base\TeleinfoModel
{
    public function getTotal($field, $period, $count)
    {
        $sql = <<<EOD
SELECT date_trunc(':period', created) AS created, sum(:field) AS :field
    FROM :relation
    GROUP BY date_trunc(':period', created)
EOD;

        $sql = strtr($sql, [
            ':count' => $count,
            ':field' => $field,
            ':period' => $period,
            ':relation' => $this->getStructure()->getRelation(),
        ]);

        return $this->query($sql);
    }
}
