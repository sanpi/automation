<?php

use \PommProject\Foundation\Pomm;
use \Silex\Provider\TwigServiceProvider;
use \Silex\Provider\WebProfilerServiceProvider;
use \Silex\Provider\UrlGeneratorServiceProvider;
use \Silex\Provider\ServiceControllerServiceProvider;
use \PommProject\Silex\ServiceProvider\PommServiceProvider;
use \PommProject\Silex\ProfilerServiceProvider\PommProfilerServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php';

if (!is_file(__DIR__ . '/config/current.php')) {
    throw new \RunTimeException('No current configuration file found in config.');
}

$app = new \Silex\Application();

$app['config'] = $app->share(function () use($app) {
    $config = require __DIR__ . '/config/current.php';
    $config['pomm']['spore']['class:session_builder'] = '\PommProject\ModelManager\SessionBuilder';

    return $config;
});

$app['pomm.configuration'] = $app->share(function () use($app) {
    return $app['config']['pomm'];
});

$app['debug'] = $app['config']['debug'];

$app['parser'] = new \Teleinfo\Parser();

$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));

$app->register(new PommServiceProvider(), $app['config']['pomm']);

$app['db'] = $app->share(function() use ($app) {
    return $app['pomm']['spore'];
});

if (class_exists('\Silex\Provider\WebProfilerServiceProvider')) {
    $app->register(new UrlGeneratorServiceProvider());
    $app->register(new ServiceControllerServiceProvider());

    $profiler = new WebProfilerServiceProvider();
    $app->register($profiler, array(
        'profiler.cache_dir' => __DIR__ . '/../cache/profiler',
    ));
    $app->mount('/_profiler', $profiler);

    $app->register(new PommProfilerServiceProvider());
}

return $app;
