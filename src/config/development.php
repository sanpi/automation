<?php

return array(
    # Force true if you use the php internal HTTP server
    'debug' => true,
    'device' => 'php://stdin',
    'pomm' => array(
        'spore' => array(
            'dsn' => 'pgsql://sanpi:3.1416@127.0.0.1:5432/domotic',
        ),
    ),
);
